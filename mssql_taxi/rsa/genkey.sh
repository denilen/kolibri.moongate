#!/bin/sh
openssl genrsa -out rsa_privatekey.pem 4096
openssl rsa -out rsa_keys.txt -text -in rsa_privatekey.pem 

rm *beam
erl -noshell -eval 'compile:file(regen_rsa), regen_rsa:do("'`sed -n -e '3,37p;' rsa_keys.txt |tr -d ':\n '`'"), init:stop().'
