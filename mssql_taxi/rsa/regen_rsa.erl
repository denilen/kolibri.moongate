-module(regen_rsa).
-compile(export_all).

mod() ->
	"00a291c2b64b1b4e42ea3e9dce8a03282f4f3efe0e44139af8eaa6004a4c1e916ed94a29480dc7288eb5726848f72776c143547896374df07b435cf5361112aaaddc492af7ca30387f4e56b10359e6fdca6a71e2dca2a9c0c8c88e3cd8e135c2750bcc83fdece582e511328abb6d87ccbb3218fc9f6f1d193dafe3e55583ba5114650a7bce90c93496f85a08a1b04a0ced00d611834ee1cfcb28357ce44f808c48db4e5eb354d349b9d7224997d817b0870a2eb1a1be60a86d7cc11acc84ff4782aa5005648a690a934b9a287facdaf5ba85d77db14059b33afe99da7c59aa40f9d8ffc120b4344c161f770484064b98c2a9be68291cc6866acd9a024e7d1e0a9991afb816c873563b06ce56a04c078052182f21355e1b983bb9fe2bb57265291646643a377f55c01b5b3fb10819b09d1e37ac3048715cb14f2d3f74ba23179752d090ec8a310861009d3d3d4f4ade60ea2c08fb3664eb0c266edc9363da4d290166ad359416e852f5fbf53cac629a32fcf5d804cdc7a7a5faae250f7156d20dbb297d71c2e4999ecabc24acebb9b6d114eae3cf550db530723b70c4a08256331d374645d055d05c7569556f9ebb8a3bec6d2adaaed43a8eff99fd3bf2b388439a6a31518d01a267c7e4321ef69729da4182f71f9a923d1a9f9eec6b7cb28a5c192eaf894e70fd9c13e69511ccceaf9b56c3af9c4b37b427abcb2a62218696cbbb".

do()->
	do(mod()).

do(M)->
	io:format("original mod:~n~p~n",[M]),
	S1 = mk([],length(M)),
	S2 = xr (M, S1),
	io:format("xored:~n~p,~n~p).~n",[S1,S2]).

chr(N) ->
	lists:nth(N+1, "0123456789abcdef").

mk(L,0)->L;
mk(L,N)->mk([chr(random:uniform(16)-1)|L], N-1).

ord(C)->
	string:chr("0123456789abcdef", C)-1.

xr(L1, L2) ->
	lists:zipwith(fun(C1,C2)->
						  chr(ord(C1) bxor ord(C2))
				  end, L1, L2).



	
