-module(sql).
-compile(export_all).

-include_lib("./config.hrl").
-include_lib("./common.hrl").


-ifdef(lic).
-define(SENS, erlang:process_flag(sensitive, true)). 
-define(CHECK, 
		First = case get(lic) of
					T when is_tuple(T)-> false;
					_ -> true
				end,
		if First ->
				timer:send_interval(check_period(), sql, check_lic);
		   true -> ok
		end,
		Lic = checker:check(),
		case Lic of 
			{_, _, NotDemo} ->
				if First and NotDemo ->
						{N,D,ND} = Lic,
						io:format("Checking license...~n",[]),
						io:format("unlimited license: ~p~n",[D]),
						if not D ->
								L = if ND -> N;
									   true -> demo_threshold()
									end,
								io:format("drivers limit: ~p~n",[L]);
						   true -> ok
						end;
				   true -> ok
				end,
				put(lic, Lic),
				if NotDemo -> 
						ok;
				   true -> %demo mode
						if First ->
								io:format("Demo mode enabled.~n",[]);
						   true ->
								io:format("Demo mode timed out; shutting down~n",[]),
								init:stop()
						end
				end;
			_ ->
				io:format("license get fail~n",[]),
				if First ->
						io:format("enabling demo mode~n",[]),
						put(lic, {0 ,false , false});
				   true ->
						case get(lic) of
							{_,_,false} ->
								io:format("Demo mode timed out; shutting down~n",[]),
								init:stop();
							_ ->
								io:format("enabling demo mode~n",[]),
								put(lic, {0 ,false , false})
						end
				end
				end).
				%io:format("------------------------------------------------------------~n",[])).

-else.
-define(SENS, ok). 
-define(CHECK, ok).
-endif.


-ifdef(lic).
l2q({DriverThreshold, DisableThreshold, NotDemo}) ->
	if NotDemo ->
			if DisableThreshold ->
					" ";
			   true ->
					" top "++integer_to_list(DriverThreshold)++" "
			end;
	   true ->
			" top "++integer_to_list(demo_threshold())++" "
	end;
l2q(_) ->
	" ".
-else.
l2q(_) ->
	" ".
-endif.

start(PPid) ->	
	?SENS,
	erlang:register(sql, self()),
	process_flag(trap_exit, true),
	?CHECK,

	Pid = spawn_link (?MODULE, loop_start, [true]),
	Pid ! {lic, get(lic)},
	receive
		start_ok -> PPid ! start_ok;
		{'EXIT', Pid, Why} ->
			error_logger:error_msg("DB conn problem: ~p~n", [Why]),
			io:format("DB conn problem: ~p~n", [Why]),
			erlang:exit({db_conn_problem, Why})
	end,
	super_loop(Pid).

super_loop(Pid) ->
	NewPid = receive
				 Q when Q=:=refresh_auth; 
						Q=:=refresh_parkings;
						Q=:=refresh_orders;
						Q=:=refresh_queue_rules -> 
					 resend(Pid, Q);
				 {Q, List} when Q=:=flush_statuses;
								Q=:=flush_distrib;
								Q=:=flush_orders_states;
								Q=:=missing_client;
								Q=:=refresh_messages;
								Q=:=update_message;
								Q=:=update_announce-> 
					 resend(Pid, {Q, List});
				 check_lic ->
					 ?CHECK,
					 Pid ! {lic, get(lic)},
					 Pid
			 end,
	super_loop(NewPid).

resend(Pid, What) ->
	Pid ! What,
	receive
		done -> Pid;
		{'EXIT', Pid, Why} ->
			error_logger:error_msg("DB conn died: ~p~n", [Why]),
			NewPid = restart_loop(),
			resend(NewPid, What)			
	end.

restart_loop() ->
	timer:sleep(loop_restart_timeout()),
	Pid = spawn_link(?MODULE, loop_start, [false]),
	Pid ! {lic, get(lic)},
	receive
		start_ok -> 
			error_logger:info_msg("DB loop process restarted"),
			Pid;
		{'EXIT', Pid, Why} ->
			error_logger:error_msg("DB conn died again: ~p~n", [Why]),
			restart_loop()
	end.

loop_start(First) ->
	?SENS,
	receive
		{lic, Lic} -> put(lic, Lic)
	end,
	odbc:start(),
	{ok,Conn} = odbc:connect(connect_string(), []),
	error_logger:info_msg("connected to DB: ~p~n",[Conn]),

	if First ->
			refresh_auth(Conn),
			timer:send_interval(auth_timeout(), sql, refresh_auth),			

			refresh_parkings(Conn),
			timer:send_interval(parkings_timeout(), sql, refresh_parkings),

			refresh_queue_rules(Conn),
			timer:send_interval(rules_timeout(), sql, refresh_queue_rules),

			{updated, _} = odbc:sql_query(Conn, "update lider set msgstatus = 1 where msgtype = 1 and msgstatus = 0 and msgdateend > getdate()"),
			LastId = refresh_messages(Conn, 0),
			erlang:send_after(messages_timeout(), sql, {refresh_messages, LastId}),

			{updated, _} = odbc:sql_query(Conn, "update cars set status = 1024"),
			{updated, _} = odbc:sql_query(Conn, "update orders set canal = 0 where canal =1 and state = 0"),

			%%{selected, _, Res} = odbc:sql_query(Conn, "select o.id, o.phone, o.addrfrom, o.housefrom, o.flatfrom, o.addrto, o.zoneid, o.dtarrive, o.descr, o.request_attributes, o.serviceid, o.sum, o.state, o.carid, o.time2meet, c.pass, c.state, c.serviceid, c.car_attributes, c.driverid from ORDERS as o join cars as c on c.id = o.carid where o.canal =1 and o.state =2"),
			{selected, _, Res} = odbc:sql_query(Conn, "select o.id, o.phone, CASE WHEN (o.AddrFromName IS NULL OR o.AddrFromName = '') THEN o.AddrFrom ELSE '('+o.AddrFromName+') '+o.AddrFrom END AS addrfrom , o.housefrom, o.flatfrom, CASE WHEN (o.AddrToName IS NULL OR o.AddrToName = '') THEN o.AddrTo ELSE '('+o.AddrToName+') '+CASE WHEN (o.HouseTo IS NOT NULL AND LEN(o.HouseTo) > 0) THEN o.AddrTo+' '+o.HouseTo ELSE o.AddrTo END END AS addrto, o.zoneid, o.dtarrive, o.descr, o.request_attributes, o.serviceid, o.sum, o.state, o.carid, o.time2meet, c.pass, c.state, c.serviceid, c.car_attributes, c.driverid from ORDERS as o join cars as c on c.id = o.carid where o.canal =1 and o.state =2"),
			cache ! {restart_hack, Res},

			timer:send_interval(orders_timeout(), sql, refresh_orders);
		
		true -> ok
	end,	
	sql ! start_ok,
	loop(Conn).

loop(Conn) ->
	receive 
		refresh_auth ->
			refresh_auth(Conn);
		refresh_parkings ->
			refresh_parkings(Conn);
		refresh_orders ->
			refresh_orders(Conn, old),
			refresh_orders(Conn, predvar),
			refresh_orders(Conn, new);		
		refresh_queue_rules ->
			refresh_queue_rules(Conn);
		{refresh_messages, LastId} ->
			NextId = refresh_messages(Conn, LastId),
			erlang:send_after(messages_timeout(), sql, {refresh_messages, NextId});

		{update_message, {MId, Count}} ->
			error_logger:info_msg("updating message: ~p~n",[MId]),
			if Count =:= null ->
					whatever;
			   true -> 
					{updated, _} = odbc:sql_query(Conn, "update lider set msgstatus = "++erlang:integer_to_list(Count)++" , msgdate = getdate() where id ="++erlang:integer_to_list(MId))
			end;
		{update_announce, {AId, Count, DReceive}} ->
			error_logger:info_msg("updating announce; message: ~p~n",[AId]),
			if Count =:= null orelse DReceive =:= null ->
					whatever;
			   true -> 
					%{updated, _} = odbc:sql_query(Conn, "update lider set msgstatus = "++erlang:integer_to_list(Count)++" , msgdate = '"++DReceive++"' where id ="++erlang:integer_to_list(AId))
					{updated, _} = odbc:sql_query(Conn, "update lider set msgstatus = "++erlang:integer_to_list(Count)++" , msgdate = getdate() where id ="++erlang:integer_to_list(AId))
			end;

		{flush_statuses, List} ->
			flush_statuses(Conn, List);

		{flush_distrib, List} ->
			flush_distrib(Conn, List);
		
		{flush_orders_states, List} ->
			flush_orders_states(Conn, List);
			
		{missing_client, {OrderId, CarId}} ->
			inform_missing_client(Conn, OrderId, CarId);

		{lic, Lic} -> 
			put(lic, Lic)
	end,
	sql ! done,
	loop(Conn).
	
refresh_orders(Conn, Part) ->
%	error_logger:info_msg("selecting orders, part:~p conn:~p~n", [Part,Conn]),
	Cond = case Part of
			   predvar -> "state = 5 and canal = 0 and datediff(minute, getdate(), dtpredvar) < " ++ integer_to_list(predvar_time());
			   new -> "state != 5 and canal = 0";
			   old -> "canal = 1"
		end,
	{selected, _, Res} = odbc:sql_query(Conn, "select id, phone, CASE WHEN (o.AddrFromName IS NULL OR o.AddrFromName = '') THEN o.AddrFrom ELSE '('+o.AddrFromName+') '+o.AddrFrom END AS addrfrom, housefrom, flatfrom, CASE WHEN (o.AddrToName IS NULL OR o.AddrToName = '') THEN o.AddrTo ELSE '('+o.AddrToName+') '+CASE WHEN (o.HouseTo IS NOT NULL AND LEN(o.HouseTo) > 0) THEN o.AddrTo+' '+o.HouseTo ELSE o.AddrTo END END AS addrto, zoneid, dtarrive, descr, request_attributes, serviceid, sum, state from ORDERS as o where "++ Cond),
%	error_logger:info_msg("got ~p orders~n", [length(Res)]),
	List = [{order, Id, Phone, AddrFrom, HouseFrom, FlatFrom, AddrTo, Parking, DtArrive, Descr, RuleHead, ServiceId, fix_int(Sum,0), State} 
			|| {Id, Phone, AddrFrom, HouseFrom, FlatFrom, AddrTo, Parking, DtArrive, Descr, RuleHead, ServiceId, Sum, State} <- Res], %UNSAFE_RECORD
	if
		Part =/= old andalso length(List) >0 ->
			UpdateQuery = case Part of 
							  predvar -> "update orders set canal = 1, state = 0 where id in (";
							  new -> "update orders set canal = 1 where id in ("
						  end,
			{updated, _} = odbc:sql_query(Conn, UpdateQuery ++
										  string:join([integer_to_list(element(1,Order)) || Order <- Res], ", ")
										  ++")"),
			cache ! {orders_new, List};
		Part =/= old andalso length(List) =:=0 -> wait_for_more;
		true -> cache ! {orders_old, List}
	end.
%	error_logger:info_msg("done selecting orders~n").

refresh_auth(Conn) ->	
	error_logger:info_msg("selecting cars~n"),
	{selected, _, Res} = odbc:sql_query(Conn, "select "++l2q(get(lic))++" c.id, c.pass, c.state, c.serviceid, c.car_attributes, c.driverid, cast(d.balance as int) as balance from cars as c join drivers as d on c.driverid = d.id where c.state in (2,3)"),
	List = [{driver, Id, hex:hexstr_to_bin(Pass), state2a(State), Service, Attributes, DriverId, fix_int(Balance, 0)} 
			|| {Id, Pass, State, Service, Attributes, DriverId, Balance} <- Res],  %UNSAFE_RECORD
	cache ! {auth, List}.
	%% error_logger:info_msg("done selecting cars~n"),

refresh_parkings(Conn) ->
	error_logger:info_msg("selecting zones~n"),
	{selected, _, Res} = odbc:sql_query(Conn, "select id, descr from zones where rad=0 and state=0"),
	cache ! {parkings, Res}.
%% error_logger:info_msg("done selecting zones~n"),

refresh_queue_rules(Conn) ->
	%%error_logger:info_msg("selecting rules~n"),
	{selected, _, Res} = odbc:sql_query(Conn, "select start_request, rules from queue_rules"),
	List = [{string:substr(Start, 1, length(Start)-1),
			 [sets:from_list(string:tokens(S,";")) || S <- string:tokens(Rule, ")(")]} 
			|| {Start, Rule} <- Res],
	cache ! {queue_rules, List}.
	%%error_logger:info_msg("done selecting rules~n"),

flush_statuses(Conn, List) ->
	%%error_logger:info_msg("flushing driver statuses~n"),
	[odbc:param_query(Conn, "update cars set status = ? where id = ?"
					  , [{sql_integer, [a2status(S)]},
						 {sql_integer, [D]}])
	 || {D, S} <- List, is_integer(D)].

flush_distrib(Conn, List) ->
	%%error_logger:info_msg("flushing driver distrib~n"),
	odbc:sql_query(Conn,
								  "update distrib set data = '["++
								  string:join(
									[string:join([integer_to_list(I) 
												  || I <- [Parking 
														   | [Driver#driver.id 
															  || Driver <- Drivers]]]
												 , " ")
									 || {Parking, Drivers} <- List, length(Drivers) > 0]
									, "][")
								  ++ "]' where id=1").
	%error_logger:info_msg("done flushing driver distrib~n").


flush_orders_states(Conn, List) ->
	%error_logger:info_msg("flushing order states~n"),
	lists:map(fun (Q) ->
					  case Q of 
						  {Id, State, CarId, DriverId, Time2meet}->
							  {R, TimeStr} = case State of 
												 3 -> {1, "dtend = getdate(), canal = 2"};
												 _ -> {0, "dtbegin = getdate() "}
											 end,
								if State =:= 2 ->
										do_notification(Conn, preliminary, Id, CarId);
								   true -> ok
								end,
							  {updated, _} = odbc:param_query(Conn, "update orders set state = ?, ResultCode = ? , carid = ?, driverid = ?, time2meet = ?, "++ TimeStr ++" where id = ?"
															  , [{sql_integer, [State]},
																 {sql_integer, [R]},
																 {sql_integer, [CarId]},
																 {sql_integer, [DriverId]},
																 {sql_integer, [Time2meet]},
																 {sql_integer, [Id]}]);
						  {Id, unusual} ->
							  error_logger:info_msg("unusual finalizing, possibly order cancelled ~p~n",[Id]),
							  {updated, _} = odbc:sql_query(Conn, "update orders set orders.canal = 2 where id = " ++ erlang:integer_to_list(Id))
					  end
			  end, List).
												%error_logger:info_msg("done flushing order states~n").

inform_missing_client(Conn, OrderId, CarId) ->
	do_notification(Conn, missing, OrderId, CarId).

do_notification(Conn, Type, OrderId, CarId)->
	{Method, Mess, Nivr} = case Type of
					   preliminary -> {client_preliminary_notification_method(), " � ��� ���-� �-� ", "1"};
					   missing -> {client_missing_notification_method(), " ��� ������� �-� ", "2"}
				   end,
	case Method of
		sms_phone -> odbc:sql_query(Conn, "insert into SMS_OUT (order_id, tel_to, status, text, tries, createTime) select o.id, o.phone,  'N', '������ ' + o.fromPhone + '"++Mess++"' + c.Mark  + ' �-� '  + c.gosnum + ' �-�� ' + d.Phones, 0, GETDATE() from ORDERS as o , cars as c, drivers as d where o.ID = "++integer_to_list(OrderId)++" and c.ID = "++integer_to_list(CarId) ++ "and d.CarId = c.ID");
		sms_no_phone -> odbc:sql_query(Conn, "insert into SMS_OUT (order_id, tel_to, status, text, tries, createTime) select o.id, o.phone,  'N', '������ ' + o.fromPhone + '"++Mess++"' + c.Mark  + ' ����� '  + c.gosnum, 0, GETDATE() from ORDERS as o , cars as c, drivers as d where o.ID = "++integer_to_list(OrderId)++" and c.ID = "++integer_to_list(CarId) ++ "and d.CarId = c.ID");
		call -> odbc:sql_query(Conn, "insert into CALL_OUT (order_id, car_id  , tel_to, status, nivr, tel_from, tries, start_time) select ID, "++integer_to_list(CarId)++" , phone, 'N', '"++Nivr++"', fromPhone, 0, GETDATE()  from ORDERS where ID = "++integer_to_list(OrderId));
		_ -> ok
	end.
		
refresh_messages(Conn, Id) ->
	%error_logger:info_msg("selecting messages, starting from ~p~n",[Id]),
	{selected, _, Res} = odbc:sql_query(Conn, "select id, carid, msgdatebegin, msgdateend, msgdate, msgtype, msgstatus, msg from lider where ((msgtype = 2 and msgdateend > getdate() and msgdatebegin < getdate()) or (msgtype != 2 and msgstatus = 0)) and id > "++erlang:integer_to_list(Id)++" order by id desc"),
	List = [{message, Idd, CarId, msgType2a(Type), fix_int(Status, 0), Begin, End, Date, Msg} ||
			   {Idd, CarId, Begin, End, Date, Type, Status, Msg} <- Res],  %UNSAFE_RECORD			
	cache ! {messages, List},	
	case List of 
		[] -> Id;
		_ -> (hd(List))#message.id
	end.


fix(Val, Pred, Default) ->
	B = Pred(Val),
	if B -> Val;
	   true -> Default
	end.

fix_int(Val, Default) ->
	fix(Val, fun erlang:is_integer/1, Default).
