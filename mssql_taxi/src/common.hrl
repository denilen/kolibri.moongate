-compile(export_all).

%records are unsafe to change; grep 'UNSAFE_RECORD' *rl
-record(driver, {id, 
				 password, 
				 state, %% work | cant_work 
				 serviceid, 
				 attributes,
				 driverid,
				 balance}).

-record(state,{
		  driver = #driver{},
		  status = free,
		  name = start, %%start, main , unreg, negative_balance
		  parking = 0, 
		  fails = 0,
		  orderid = 0,
		  offers =[],
		  lasttake = 0,
		  lastMessage = no_message}).

-record(order, {id, 
				phone,
				addrfrom, 
				housefrom,
				flatfrom,
				addrto, 
				parking, 
				dtarrive, 
				descr, 
				rule_head, 
				serviceid,
				sum,
				state}).

-record(message, {id,
				  carid,
				  type,
				  count,
				  dBegin,
				  dEnd,
				  dReceive,
				  text}).

msgType2a(1) -> leader;
msgType2a(2) -> announce;
msgType2a(3) -> message.

a2msgType(leader) -> 1;
a2msgType(announce) -> 2;
a2msgType(message) -> 3.
	

state2a(State) ->
	case State of 
		0 -> no_term_no_smena;  %%can't login
		1 -> no_terminal_smena; %%can't login
		2 -> cant_work;   %%can login, cant't take orders
		3 -> work
	end.

a2status(AStatus) ->
	case AStatus of
		offline -> 1024;
		free -> 0;
		working -> 4;
		order_finished -> 5;
		missing_client -> 7
	end.

status2a(Status) ->
	case Status of 
		0 -> free;
		4 -> working;
		5 -> order_finished;
		7 -> missing_client;
		_ -> free
	end.
			 

a2ostate(OrderState) ->
	case OrderState of 
		searching -> 1;
		execution -> 2;
		finished -> 3
	end.


	
cursecs()->
	calendar:datetime_to_gregorian_seconds(erlang:localtime()) -
		calendar:datetime_to_gregorian_seconds({{1970,1,1},{0,0,0}}).

time2usecs(Time)->
	calendar:datetime_to_gregorian_seconds(hd(calendar:local_time_to_universal_time_dst(Time))) -
		calendar:datetime_to_gregorian_seconds({{1970,1,1},{0,0,0}}).

str2time({{Y, Mo, D}, {H, Mi, Se}}) -> {{Y, Mo, D}, {H, Mi, Se}};
str2time(S) ->
	case erlang:system_info(otp_release) of
		[$R,$1,$4|_] ->
			S;
		_ ->
			[Y1, Mo1, D1, H1, Mi1, S1|_] = string:tokens(S, "-: ."),
			[Y, Mo, D, H, Mi, Se] = 
				[list_to_integer(T) || T <- [Y1, Mo1, D1, H1, Mi1, S1]],
			{{Y, Mo, D}, {H, Mi, Se}}
	end.

cursecs2str(C) ->
	time2str(calendar:gregorian_seconds_to_datetime(C+calendar:datetime_to_gregorian_seconds({{1970,1,1},{0,0,0}}))).

curr_time_add(Time) ->
	NowSecs = calendar:datetime_to_gregorian_seconds(
				erlang:localtime()),
	NewSecs = NowSecs + Time *60,
	calendar:gregorian_seconds_to_datetime(NewSecs).

time2str({{Y, Mo, D},{H, Mi, S}}) ->
	integer_to_list(Y) 
		++"-"++
		integer_to_list(Mo)
		++"-"++
		integer_to_list(D)
		++" "++
		integer_to_list(H)
		++":"++
		integer_to_list(Mi)
		++":"++
		integer_to_list(S)
		++".000".

code2time(C) ->
	case C of 
		0 -> refuse;
		1 -> 5;
		2 -> 10;
		3 -> 15
	end.

time2code(T) ->
	case T of 
		refuse -> 0;
		5 -> 1;
		10 -> 2;
		15 -> 3;
		_ -> 0
	end.

%% drivers_auth {#driver}

%% drivers_online {id, pid | disconnected, State, When}

%% parkings {id, name}

%% drivers_statuses {id, status}

%% drivers_distrib {pakringId, Orders, Drivers}

%% orders {id, pid, order}

%% rules {start, set(rules)}

%% orders_states 
%% {order_id,
%%  state,
%%  dtbegin | dtend,
%%  car_id
%%  time2meet
%% }|{Id, unusual}
