-module(test).
-compile(export_all).

-include_lib("./common.hrl").
-include_lib("./config.hrl").


shit(Prkng)->
	[{_,Ords,Drs}] = ets:lookup(drivers_distrib,Prkng),
	Heads = [O#order.rule_head || O <- Ords],
	Drs1 = [{D#driver.id, D#driver.attributes} || D <- Drs],
	io:format("heads: ~p~n Drivers~p~n",[Heads, Drs1]),
	lists:map(fun(Q) ->trace_selection(Q,Drs1) end, lists:usort(Heads)).

trace_selection_h(Rule, [], Acc)->
	Acc;
	
trace_selection_h(Rule, Attrs, Acc)->	
	Sel = select_driver(Rule, Attrs),
	trace_selection_h(Rule, [A||A<-Attrs, A=/=Sel],Acc++[Sel]).
	
trace_selection(Head, Attrs)->
	[{_,Rule}] = ets:lookup(rules, Head),
	Acc = trace_selection_h(Rule, Attrs, []),
	io:format("~p - ~p~n",[Head, Acc]).
	
	
select_driver([], Drivers) ->
	io:format("broken rules: rule does not cover all (service, plan) pairs; selecting first driver"),
	hd(Drivers);

select_driver([Rule|OtherRules] , Drivers) ->	
	case [Driver || Driver <- Drivers, sets:is_element(element(2,Driver), Rule)] of
		[] -> 
			%error_logger:info_msg("rule skip Drivers~p Rule~p~n",[Drivers,sets:to_list(Rule)]),
			select_driver(OtherRules, Drivers);
		[Driver|OtherDrivers] ->
			%error_logger:info_msg("rule select Drivers~p Rule~p~n",[Driver,sets:to_list(Rule)]),
			Driver
	end.

%------------------------------------------------------------

conn()->
	odbc:start(),
	{ok, Conn} = odbc:connect(connect_string(),[]),
	Conn.

sql()->
	Cond = "canal = 1",
	{selected, _, Res} = odbc:sql_query(conn(), "select id, phone, addrfrom, housefrom, flatfrom, addrto, zoneid, dtarrive, descr, request_attributes, serviceid, sum, state from ORDERS where "++ Cond),
	io:format("~p~n",[Res]).

refresh_orders(Conn, Part) ->
	error_logger:info_msg("selecting orders, part:~p conn:~p~n", [Part,Conn]),
	Cond = case Part of
			   predvar -> "state = 5 and canal = 0 and datediff(minute, getdate(), dtpredvar) < " ++ integer_to_list(predvar_time());
			   new -> "state != 5 and canal = 0";
			   old -> "canal = 1"
		end,
	{selected, _, Res} = odbc:sql_query(Conn, "select id, phone, addrfrom, housefrom, flatfrom, addrto, zoneid, dtarrive, descr, request_attributes, serviceid, sum, state from ORDERS where "++ Cond),
	error_logger:info_msg("got ~p orders~n", [length(Res)]),
	List = [{order, Id, Phone, AddrFrom, HouseFrom, FlatFrom, AddrTo, Parking, DtArrive, Descr, RuleHead, ServiceId, Sum, State} 
			|| {Id, Phone, AddrFrom, HouseFrom, FlatFrom, AddrTo, Parking, DtArrive, Descr, RuleHead, ServiceId, Sum, State} <- Res], %UNSAFE_RECORD
	if
		Part =/= old andalso length(List) >0 ->
			UpdateQuery = case Part of 
							  predvar -> "update orders set canal = 1, state = 0 where id in (";
							  new -> "update orders set canal = 1 where id in ("
						  end,
			{updated, _} = odbc:sql_query(Conn, UpdateQuery ++
										  string:join([integer_to_list(element(1,Order)) || Order <- Res], ", ")
										  ++")"),
			cache ! {orders_new, List};
		Part =/= old andalso length(List) =:=0 -> 
			wait_for_more;
		true -> 
			error_logger:info_msg("shit1~n"),
			cache ! {orders_old, List},
			error_logger:info_msg("shit2~n")
	end,
	error_logger:info_msg("done selecting orders~n").

%------------------------------------------------------------

timeshit()->
	time2usecs(str2time("2020-10-10 00:00:00.000")).

timeshit2()->
	cursecs().

