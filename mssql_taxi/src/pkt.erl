-module(pkt).
-export([auth_error/2,
		 parkings_list/1,
		 parkings_info/2,
		 balance/2,
		 order_offer/3,
		 order_confirm/4,
		 order_cancelled/2,
		 message/2,
		 finitor/1]).

-include_lib("./common.hrl").
-include_lib("./config.hrl").

wrap_load(Head, ServerOpcode, Load) ->
	<<Head/binary, ServerOpcode:8, (size(Load)):16/little, Load/binary, <<170:8,170:8>>/binary>>.

auth_error(Head, Reason) ->
	wrap_load(Head, 1, <<(auth_error_reason2int(Reason)):8>>).

auth_error_reason2int(bad_pass)->0;
auth_error_reason2int(bad_version) ->1;
auth_error_reason2int(double_login) ->2;
auth_error_reason2int(bad_badmatch) ->3.


parkings_list(Head) ->
	Parkings = cache:get_parkings(),
	wrap_load(Head, 2, 
			  << <<Id:8, 0:8,<<(list_to_binary(Name))/binary, 0:80>>:10/binary>> || {Id, Name} <- Parkings>>).

parkings_info(Head, State) ->
	Distrib = cache:get_drivers_distrib(),
	DriverService = (State#state.driver)#driver.serviceid,
	wrap_load(Head, 3,
			  list_to_binary(lists:map(fun({Parking, OrdersNum, Drivers}) ->
								DriversFiltered = case show_all_drivers() of
													   true -> [Driver#driver.id 
															 || Driver <- Drivers];
													   _ -> [Driver#driver.id 
															 || Driver <- Drivers, Driver#driver.serviceid =:= DriverService]
												   end,
							  <<(if  
									 State#state.parking =:= Parking -> 1;
									 true -> 0
								 end):8, 
								Parking:8,
								(length(DriversFiltered)):8,
								OrdersNum:8,
								(<< <<DriverId:16/little>> 
									|| DriverId <- DriversFiltered>>)/binary>>
									   end
									   , Distrib))) .

balance(Head, Balance) ->
	case Balance of
		null -> 
			error_logger:error("sending balance failed: null balance~n",[]),
			<<>>;
		_ ->
			BalBin = erlang:list_to_binary( erlang:integer_to_list(Balance)),
			wrap_load(Head, 8,
				<<(size(BalBin)):8,
					BalBin/binary>>)
		end.
	

order_offer(Head, Order, Preview) ->
	[ResBin, AddrBin, CommentBin] = 
		lists:map(fun erlang:list_to_binary/1, 
				  [Order#order.addrfrom ++" "++ Order#order.housefrom ++" "++ Order#order.flatfrom, 
				   " ",
				   "������: " ++ Order#order.rule_head ++ " " ++ Order#order.descr]),
	Secs = time2usecs(str2time(Order#order.dtarrive)),
	OrderState = case Preview of
					 false -> 0;
					 true -> 5
				 end,
	DriverAnswer = 0,
	wrap_load(Head, 7,
			  <<(size(ResBin)):24,
				ResBin/binary,
				(size(AddrBin)):24,
				AddrBin/binary,												 
				0:24,
				OrderState:4,
				DriverAnswer:4,												 
				Secs:32,											 
				(size(CommentBin)):24,
				CommentBin/binary,
				0:16,
				(Order#order.id):32>>).

order_confirm(Head, Order, A, TimeTakenSecs) ->
	[ResBin, AddrBin, CommentBin] = 
		lists:map(fun erlang:list_to_binary/1, 
				[Order#order.addrfrom ++" "++ Order#order.housefrom ++" "++ Order#order.flatfrom, 
				   Order#order.addrto,
				   "������: " ++ Order#order.rule_head ++ " " ++ Order#order.descr ++ " ���." ++ Order#order.phone ++ " �����:" ++ erlang:integer_to_list(Order#order.sum)]),
	
	OrderState = case A of
					 refuse -> 1;
					 not_confirmed -> 3;
					 _ -> 2
				 end,
	DriverAnswer = time2code(A),	
	SecsArrive = time2usecs(str2time(Order#order.dtarrive)),
	wrap_load(Head, 7,
			  <<(size(ResBin)):24,
				ResBin/binary,
				(size(AddrBin)):24,
				AddrBin/binary,					
				0:24,
				OrderState:4,
				DriverAnswer:4,												 
				%SecsArrive:32,
				TimeTakenSecs:32,
				(size(CommentBin)):24,
				CommentBin/binary,
				0:16,
				(Order#order.id):32/little>>).

order_cancelled(Head, Order) ->
	order_confirm(Head, Order, refuse, cursecs()).

order_not_confirmed(Head, Order) ->
	order_confirm(Head, Order, not_confirmed, cursecs()).

message(Head, Mess) ->
	Bin = list_to_binary(Mess#message.text),
	wrap_load(Head, 9,
			  <<(a2msgType(Mess#message.type)):8,
				(size(Bin)):8,
				Bin/binary>>).
				
finitor(Head) ->
	wrap_load(Head, 10, <<>>).

