-module(hex).
%-compile(export_all).
-export([bin_to_hexstr/1, hexstr_to_bin/1, hashCheap/1, hashCheapToExpensive/1]).
-include_lib("./config.hrl").

bin_to_hexstr(Bin) ->
	lists:flatten([io_lib:format("~2.16.0B", [X]) ||
					  X <- binary_to_list(Bin)]).

hexstr_to_bin(S) ->
	hexstr_to_bin(S, []).
hexstr_to_bin([], Acc) ->
	list_to_binary(lists:reverse(Acc));
hexstr_to_bin([X,Y|T], Acc) ->
	{ok, [V], []} = io_lib:fread("~16u", [X,Y]),
	hexstr_to_bin(T, [V | Acc]).

hash(Prev, 0)->
	Prev;
hash(Prev, N)->
	Q = crypto:sha(<<Prev/binary,(salt())/binary>>),
	hash(Q,N-1).

%%binary->binary
hash(Pass)->
	hash(Pass, hash_repeat()).

hashCheap(Pass)->
	hash(Pass, 10000).

hashCheapToExpensive(Pass)->
	hash(Pass, 90000).
