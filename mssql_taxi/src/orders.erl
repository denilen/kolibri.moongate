-module(orders).
-compile(export_all).

-include_lib("./common.hrl").
-include_lib("./config.hrl").

init(Order) ->
	cache:do_register_order(Order),
	start(Order, []).
	
filter_refusals(Ref) ->
	CurTime = cursecs(),
	[{Id, Secs} || {Id, Secs} <- Ref, (CurTime - Secs < refusal_timeout())].

filterout_refusals(Drivers, Refusals) ->
	[Driver || Driver <- Drivers, 
			   not(lists:any(fun({Id, _}) ->
									 Driver#driver.id =:= Id 
							 end, Refusals))].

drivers_queue_econom(All) ->
	NotEconom = [D || D <- All,
					  not($K=:=hd(lists:reverse(D#driver.attributes)))],
	case NotEconom of 
		[] ->
			All;
		NE ->
			NE
	end.

drivers_queue(Parking, Refusals) ->
	All = cache:get_parkings_drivers(Parking),
	ToFilter = case econom_logic() of
				   true ->
					   drivers_queue_econom(All);
				   false ->
					   All
			   end,
	filterout_refusals(ToFilter, Refusals).			

start(Order, Refusals) ->
	receive
		cancelled ->
			cache:do_unregister_order(Order),
			cache:set_unusual_order_state(Order#order.id),
			cache:do_remove_order(Order#order.id);
		_ ->
			start(Order, Refusals)				
	after 0 ->
			NewOrder = case cache:get_new_order(Order#order.id) of
						   Order -> 
							   %%unchanged,
							   Order;
						   Q when erlang:is_record(Q, order) -> 
							   error_logger:info_msg("update Order: ~p~n",[Order#order.id]),
							   Q;
						   _ -> 
							   error_logger:error_msg("order cant find self in orders table, proceeding without update; Order: ~p~n", [Order]),
							   Order
					   end,
			case drivers_queue(NewOrder#order.parking, Refusals) of 
				[] ->
					wait_for_drivers(NewOrder, Refusals);
				Drivers ->
					case select_driver(cache:get_rule(NewOrder#order.rule_head), Drivers) of
						no_driver ->
							wait_for_drivers(NewOrder, Refusals);
						Driver ->								 
							try_offer(NewOrder, Driver, Refusals)						
					end
			end
	end.

wait_for_drivers(Order, Refusals)->
	timer:sleep(no_drivers_timeout()),			
	start(Order, filter_refusals(Refusals)).

select_driver(all, _Drivers) ->
	no_driver;

select_driver([], _Drivers) ->
	no_driver;

select_driver([Rule|OtherRules] , Drivers) ->	
	case [Driver || Driver <- Drivers, sets:is_element(Driver#driver.attributes, Rule)] of
		[] -> 			
			select_driver(OtherRules, Drivers);
		[Driver|OtherDrivers] ->
			Driver
	end.

try_offer(Order, Driver, Refusals) ->
	error_logger:info_msg("trying to offer Order: ~p to Driver: ~p~n",[Order#order.id, Driver#driver.id]),
	case do_offer(Order, Driver) of 
		timeout -> 
			error_logger:info_msg("refuse by timeout: Driver: ~p Order: ~p~n",[Driver#driver.id,Order#order.id]),
			case cache:get_driver_proc(Driver#driver.id) of
				Pid when is_pid(Pid) ->
					Pid ! {remove_offer, Order};
					_ -> whatever
				end,
			%%cache:do_reregister_driver(Order#order.parking, Order#order.parking, Driver),
			start(Order, [{Driver#driver.id, cursecs()}| Refusals]);
		refuse -> start(Order, [{Driver#driver.id, cursecs()}| Refusals]);
		{taken, Time, When} -> 
			case cache:get_driver_proc(Driver#driver.id) of
				Pid when is_pid(Pid) ->
					Pid ! {acc_offer, Order},

					receive 
						acc_offer_ok ->							
							error_logger:info_msg("Order: ~p taken by Driver: ~p~n", [Order#order.id, Driver#driver.id]),
							cache:do_unregister_order(Order),
							cache:set_order_state(Order, Driver, Time, 2),
							erlang:send_after(execution_timeout(), self(), execution_timeout),
							taken(Order, Driver, Time, When)
					after 10000 ->
							error_logger:warning_msg("can't wait for confirmation of reception of order acception confirmation from client process; Driver: ~p Order: ~p~n",[Driver#driver.id, Order#order.id]),
							start(Order, [{Driver#driver.id, cursecs()}| Refusals])
					end;
				
				_ -> 
					
					start(Order, [{Driver#driver.id, cursecs()}| Refusals])
			end		
	end.

%this clause is merely a reminiscent of a bug conserning broken rules
do_offer(Order, Driver) when not(erlang:is_record(Driver, driver)) ->
	error_logger:warning_msg("Bug: Driver is not a record, refusing. Driver: ~p; Order: ~p~n",[Driver, Order]),
	refuse;

do_offer(Order, Driver) when erlang:is_record(Driver, driver) ->
	case cache:get_driver_proc(Driver#driver.id) of
		Pid when is_pid(Pid) ->
			Token = make_ref(),
			Pid ! {offer, Order, Token},
			DrId =  Driver#driver.id,
			receive
				{answer, refuse, _, Token} -> refuse;
				{answer, Time, When, DrId, Token} -> {taken, Time, When}
			after 
				offer_timeout() -> timeout
			end;
		_ -> 
			error_logger:error_msg("trying to find driver process failed Driver: ~p Order: ~p~n",[Driver#driver.id,Order#order.id]),
			refuse
	end.	

taken(Order, Driver, Time, When) ->	
	receive
		finished ->			
			error_logger:info_msg("Order: ~p finished by Driver: ~p~n",[Order#order.id, Driver#driver.id]),
			cache:set_order_state(Order, Driver, Time, 3),
			cache:do_remove_order(Order#order.id);
		cancelled ->
			error_logger:info_msg("Order: ~p cancelled after take by Driver: ~p~n",[Order#order.id, Driver#driver.id]),
			case ets:lookup(drivers_online, Driver#driver.id) of 
				[{_, Pid, State, OfflineSince}] ->
					NewState =  State#state{orderid=0, lasttake=0, parking=0, name=unreg, status=free},	
					case Pid of
						Pid when is_pid(Pid) ->
							cache:set_driver_status((State#state.driver)#driver.id, free),			
							cache:set_driver_online((State#state.driver)#driver.id, Pid, NewState, 0),			
							Pid ! {order_cancelled, NewState, Order};				
						Pid when Pid=:=disconnected -> 
							cache:set_driver_online((State#state.driver)#driver.id, Pid, NewState, OfflineSince)
					end;					
				_ ->														
					error_logger:info_msg("cancelled Order: ~p cannot find Driver: ~p to inform; ololo~n",[Order#order.id, Driver#driver.id])
			end,
			cache:do_unregister_order(Order),
			cache:set_unusual_order_state(Order#order.id),
			cache:do_remove_order(Order#order.id);
		{driver_reconnect, NewPid} ->
			NewPid ! {resend_order, Order, Time, When},
			taken(Order, Driver, Time, When);
		execution_timeout ->
			error_logger:error_msg("Order: ~p isn't finished in ~p ms; order process killed",[Order#order.id, execution_timeout()]),
			self() ! cancelled,					
			taken(Order, Driver, Time, When)
			%do_something
	end.
	
