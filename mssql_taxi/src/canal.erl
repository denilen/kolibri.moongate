-module(canal).
-compile(export_all).

-include_lib("./common.hrl").
-include_lib("./config.hrl").

start() ->
	io:format("~nMoonGate version ~p starting...~n",[version()]),
	%io:format("~n~n",[]),
	io:format("~nCopyright (C) 2009-2014, Yakushev Denis, dennilen@gmail.com~n",[]),
	io:format("------------------------------------------------------------~n",[]),

	error_logger:logfile({open, "./logs/"++erlang:integer_to_list(calendar:datetime_to_gregorian_seconds(erlang:localtime())) ++".log"}),
	error_logger:tty(false),
	
	Pid = spawn_link(cache, start, [self()]),
	receive
		start_ok -> ok;
		{'EXIT', Pid, Why} -> 								  
			error_logger:error_msg("cache start fail: ~p~n", [Why]),
			io:format("cache start fail: ~p~n", [Why]),
			erlang:exit({cache_start_fail, Why})
	end,
	crypto:start(),
	spawn(fun tcp_server_start/0).

-ifdef(lic).
-define(TV, ok).
-else.
-define(TV, observer:start()).
%-define(TV, ok).
-endif.

tcp_server_start() ->
	erlang:register(canal, self()),
	case gen_tcp:listen(port(), [binary
							  % inet, 
							  % {ip, "127.0.0.1"}, 
							  ,{backlog, 1000}
							  ]) of
		{ok, LSock} ->		
			io:format("Listening on port: [ ~p ]~n",[port()]),
			error_logger:info_msg("Listening on port: [ ~p ] : ~p~n",[port(), LSock]),
			process_flag(trap_exit, true),
			?TV,
			tcp_server_loop(LSock);
		{error, Why} -> 
			io:format("Binding listen socket on port: [ ~p ] fail: ~p~n", [port(), Why]),
			exit(Why)
	end.

tcp_server_manage_message()->
	receive 
		{'EXIT', _, Reason} when 
			  Reason =:= normal;
			  Reason =:= auth_failed;
			  Reason =:= tcp_closed;
			  Reason =:= tcp_error;
			  Reason =:= client_timeout;
			  Reason =:= disconnect ->
			ok;
		{'EXIT', WorkerPid, Reason} -> 
			case extreme_cleanup(WorkerPid) of
				no_pid -> error_logger:error_msg("got unknown exit, reason: ~p~n", [Reason]);
				_ -> error_logger:error_msg("got tcp_worker_loop exit, reason: ~p~n",[Reason])
			end,
			ok;
		{tcp_closed, Sock} ->
			error_logger:info_msg("tcp socket closed prematurely~n",[]),
			ok
	after 0 -> 
			all
	end.

tcp_server_manage_messages()->
	case tcp_server_manage_message() of
		ok -> tcp_server_manage_message();
		all -> ok
	end.
			

tcp_server_loop(LSock) ->
	case gen_tcp:accept(LSock) of
		{ok, Sock} ->
			Ref = erlang:make_ref(),
			Pid = spawn_link(fun() ->
								receive 
									{permission, Ref} ->
										inet:setopts(Sock, [binary, 
															{active, true},
															{packet, 0},
															{keepalive, true}
															%%,{nodelay, true}
														   ]),
										%%pman:proc(self()),
										tcp_worker_loop(Sock, #state{}, <<>>);
									{no_socket, Reason} ->
										ok										
								end
						end),
			case gen_tcp:controlling_process(Sock, Pid) of
				ok -> Pid ! {permission, Ref};
				{error, Reason} -> Pid ! {no_socket, Reason}
			end;
		E -> 
			error_logger:error_msg("accepting socket error: ~p~n",[E])
	end,				
	tcp_server_manage_messages(),
	tcp_server_loop(LSock).

worker_finalize(Sock, State, Reason) ->
	cleanup_disconnected(State),
	gen_tcp:close(Sock),
	exit(Reason).

tcp_worker_loop(Sock,State,Rest) ->
	{NewState, NewRest} = 
		receive
			disconnect ->
				if State#state.orderid =:= 0 ->
						error_logger:info_msg("Disconnect forced for Driver: ~p; closing connection~n",[((State#state.driver)#driver.id)]),
						worker_finalize(Sock, State, disconnect);
				   true ->
						error_logger:info_msg("Disconnect forced for Driver: ~p while on order; ignoring~n",[((State#state.driver)#driver.id)]),
						{State, Rest}
				end;

			{new_auth, Driver} ->				
				case {(State#state.driver)#driver.state, Driver#driver.state, State#state.orderid} of
					{work, cant_work, 0} ->
						cache:do_unregister_driver(State#state.parking, State#state.driver),
						gen_tcp:send(Sock,
									 pkt:message(<<170:8, 9:16/little, 
												   ((State#state.driver)#driver.id):16/little>>, 
												 #message{id=0,
														 text="���� �� �����.",
														  type = leader})),
						{State#state{name = negative_balance, parking=0, driver = Driver}, Rest};					
					{cant_work, work, _} ->
						gen_tcp:send(Sock,
									 pkt:message(<<170:8, 9:16/little, 
												   ((State#state.driver)#driver.id):16/little>>, 
												 #message{id=0,
														  text="�� �����.",
														  type = message})),
						{State#state{name = unreg, parking =0, driver = Driver}, Rest};
					_ ->
						{State#state{driver = Driver}, Rest}
				end;				
			auth_err ->
				case inet:peername(Sock) of
					{ok, {Addr, Port}} ->
						error_logger:info_msg("Auth failed; closing connection from ~p:~p~n",[Addr, Port]);
					_ -> 
						error_logger:info_msg("Auth failed; closing connection from unknown~n",[])
				end,
				worker_finalize(Sock, State, auth_failed);
			{tcp_closed, Sock} ->
				error_logger:info_msg("socket closed by client, Driver: ~p~n",[((State#state.driver)#driver.id)]),
				worker_finalize(Sock, State, tcp_closed);
			{tcp_error, Sock, Reason} ->
				error_logger:info_msg("tcp error:~p; Driver: ~p~n", [Reason, (State#state.driver)#driver.id]),
				worker_finalize(Sock, State, tcp_error);
			Mess when is_record(Mess, message) andalso State#state.lastMessage =:= no_message ->
				error_logger:info_msg("sending message; Driver: ~p Message: ~p~n",[(State#state.driver)#driver.id, Mess#message.id]),
				gen_tcp:send(Sock,
							 pkt:message(<<170:8, 9:16/little, 
										   ((State#state.driver)#driver.id):16/little>>, Mess)),
				{State#state{lastMessage = Mess},Rest};
			{offer, Order, Token} ->
				NewSt = case lists:keyfind(Order#order.id, 1, State#state.offers) of
							   false ->
								   case (cursecs() - row_timeout()) of
									   Q when Q > State#state.lasttake, State#state.orderid =:= 0, Order#order.parking =:= State#state.parking ->
										   error_logger:info_msg("sending offer for Order: ~p to Driver: ~p~n",[Order#order.id, (State#state.driver)#driver.id]),
										   Res=gen_tcp:send(Sock, 
															pkt:order_offer(<<170:8, 9:16/little, 
																			  ((State#state.driver)#driver.id):16/little>>, 
																			Order, false)),
										   error_logger:info_msg("sending offer for Order: ~p to Driver: ~p resulted in ~p~n",[Order#order.id, (State#state.driver)#driver.id,Res]),
										   State#state{offers = [{Order#order.id, Token} | State#state.offers]};
									   _ -> 
										   error_logger:info_msg("sending offer refused Order: ~p Driver: ~p~n", [Order#order.id, (State#state.driver)#driver.id]),
										   State
								   end;
							   _ -> 
								   error_logger:warning_msg("order tried to double offer; Order: ~p~n",[Order#order.id]),
								   State								   
						   end,									
				{NewSt, Rest};
			%% passive remove by timeout
			{remove_offer, Order} ->
				case lists:keyfind(Order#order.id, 1 ,State#state.offers) of 
					false -> 
						{State, Rest};
					_ -> 
						cache:do_reregister_driver(State#state.parking, State#state.parking, State#state.driver),
						{State#state{offers = 
									 [{OrderId, Token} || {OrderId, Token} <- State#state.offers, OrderId =/= Order#order.id]},
						 Rest}
				end;
				
			{resend_order, Order, Time, WhenTaken} ->
				error_logger:info_msg("sending order confirmation: Driver: ~p  Order: ~p~n",[(State#state.driver)#driver.id,Order#order.id]),
				gen_tcp:send(Sock,
							 pkt:order_confirm(<<170:8, 9:16/little, 
												 ((State#state.driver)#driver.id):16/little>>, 
											   Order, Time, WhenTaken)),
				NewSt = State#state{orderid = Order#order.id, status = working},
				cache:set_driver_status((State#state.driver)#driver.id, working),				
				cache:set_driver_online((State#state.driver)#driver.id, self(), NewSt, 0),
				{NewSt, Rest};
			{order_cancelled, NewSt, Order} ->
				error_logger:info_msg("got order_cancelled message from orders process; Driver: ~p  Order: ~p~n",[(NewSt#state.driver)#driver.id,Order#order.id]),
				gen_tcp:send(Sock,
							 pkt:order_cancelled(<<170:8, 9:16/little, 
												   ((State#state.driver)#driver.id):16/little>>, Order)),
				{NewSt, Rest};
			{tcp, Sock, Bytes} ->
				split_packets(<<Rest/binary,Bytes/binary>>, State, Sock)
		after client_timeout() ->
				error_logger:info_msg("client timeouted; Driver: ~p~n",[(State#state.driver)#driver.id]),
				worker_finalize(Sock, State, client_timeout)
		end,
	cache:set_driver_online((State#state.driver)#driver.id, self(), NewState, 0),
	tcp_worker_loop(Sock, NewState, NewRest).


cleanup_disconnected(State) ->
	error_logger:info_msg("performing cleanup for disconnected Driver: ~p~n",[(State#state.driver)#driver.id]),
	if is_integer((State#state.driver)#driver.id) ->
			cache:set_driver_online((State#state.driver)#driver.id, disconnected, State, cursecs()),
			cache:set_driver_status((State#state.driver)#driver.id, offline);
	   true -> 
			ok
	end.
		
extreme_cleanup(Pid) ->	
	case ets:match(drivers_online, {'$1', Pid, '$2', '_'} ) of
		[] -> 
			error_logger:info_msg("performing cleanup extreme; no driver in drivers_online found for Pid ~p~n",[Pid]),
			no_pid;
		[[DriverId, State]|_] ->
			error_logger:info_msg("performing cleanup extreme; Driver: ~p~n",[DriverId]),
			cache:set_driver_online(DriverId, disconnected, State, cursecs()),
			cache:set_driver_status(DriverId, offline),
			ok				
	end.		

split_packets(<<Bytes/binary>>, State, Sock) when size(Bytes) < 10 ->
	{State, <<Bytes/binary>>};

split_packets(<<170:8,
				Useless:5/binary,
				LoadSize:16/little,
				Load/binary>>,
			  State, Sock) when size(Load) < LoadSize +2 ->
	{State, <<170:8,
			  Useless:5/binary,
			  LoadSize:16/little,
			  Load/binary>>};

split_packets(<<170:8,
				Useless:5/binary,
				LoadSize:16/little,
				Load:LoadSize/binary,
				170:8,
				170:8,
				Rest/binary>>, 
			  State, Sock) ->
	Packet = <<170:8,
			   Useless/binary,
			   LoadSize:16/little,
			   Load/binary,
			   170:8,
			   170:8>>,
	{Response, NewState} = process_packet(Packet, State),
	cache:set_driver_online((State#state.driver)#driver.id, self(), NewState, 0),
	case Response of
		void -> 
			ok;
		_ -> 
			case gen_tcp:send(Sock, Response) of
				{error, Reason} ->
					error_logger:error_msg("Problem sending packet to Driver: ~p - ~p~n",[(NewState#state.driver)#driver.id, Reason]);
				ok -> 
					ok
			end
	end,
	split_packets(Rest, NewState, Sock);

split_packets(<<_:1/binary, Rest/binary>>, State, Sock) ->
	split_packets(Rest, State, Sock).
				  

reauth(DriverID, Pass, Head, State, TryAgain)->	
	case cache:get_driver_auth(DriverID, Pass) of
		{ok, Driver, OldState} -> 
			NS = if OldState#state.name =:= negative_balance ->
						 OldState#state{driver = Driver, offers = [], name = unreg};
					true ->
						 OldState#state{driver = Driver, offers = []}
				 end,
			{pkt:parkings_list(Head), NS};
		fail -> 
			if TryAgain ->
					reauth(DriverID, hex:hashCheapToExpensive(Pass), Head, State, false);
			   true ->					
					error_logger:info_msg("bad auth pair from Driver: ~p pass: ~p~n", [DriverID, hex:bin_to_hexstr(Pass)]),
					self() ! auth_err,
					{pkt:auth_error(Head, bad_pass), State}
			end;
		double_login ->
			error_logger:info_msg("auth fail: second login for Driver: ~p~n", [DriverID]),
			self() ! auth_err,
			{pkt:auth_error(Head, double_login), State};
		{negative_balance, Driver, _} -> 
			{pkt:parkings_list(Head), 
			 State#state{driver = Driver, name = negative_balance}}
	end.
	
process_packet(<<170:8,
				 Department:16/little,
				 DriverID:16/little,
				 Opcode:8,
				 LoadSize:16/little,
				 Load:LoadSize/binary,
				 Crc:16/little>>,			   
			   State) ->
	Head = <<170:8, Department:16/little, DriverID:16/little>>,
	case {Opcode, State#state.name} of 
		{1, start} -> 
			case Load of
				<<MajorVersion:8, MinorVersion, ImeiLen:16/little, Imei:ImeiLen/binary, BPass/binary>> ->					
					error_logger:info_msg("auth packet from Driver: ~p ; version: ~p.~p; IMEI: ~p~n",[DriverID, MajorVersion, MinorVersion, binary_to_list(Imei)]),
					if (MajorVersion =/= ?CLIENT_MAJOR_VERSION) ->
							error_logger:info_msg("bad auth packet from Driver: ~p ; version: ~p.~p; required version: ~p.x~n",[DriverID, MajorVersion, MinorVersion, ?CLIENT_MAJOR_VERSION]),
							self() ! auth_err,
							{pkt:auth_error(Head, bad_version), State};
					   true ->	
							reauth(DriverID, hex:hashCheap(BPass), Head, State, both_hashes())
					   end;
				_ ->
					error_logger:info_msg("bad auth packet from Driver: ~p~n",[DriverID]),
					self() ! auth_err,
					{pkt:auth_error(Head, bad_badmatch), State}						
			end;

		{5, main} when Load =:= <<0:8>> -> 
			error_logger:info_msg("unregistration from parking: ~p ; Driver: ~p~n",[State#state.parking, State#state.driver]),
			cache:do_unregister_driver(State#state.parking, State#state.driver),
			NewState = State#state{name = unreg, parking=0},
			{pkt:parkings_info(Head, NewState), NewState };

		%% {5, main} when Load =/= <<0:8>> ->
		%% 	<<NewParking:8>> = Load,
		%% 	cache:do_reregister_driver(State#state.parking, NewParking, State#state.driver),     
		%% 	NewState = State#state{parking=NewParking},
		%% 	{pkt:parkings_info(Head, NewState), NewState};
		
		{5, Name} when Name =:= main,  Load =/= <<0:8>>, State#state.orderid =:=0, (State#state.driver)#driver.state =:= work; 
					   Name =:= unreg, Load =/= <<0:8>>, State#state.orderid =:=0, (State#state.driver)#driver.state =:= work  ->
			<<NewParking:8>> = Load,
			error_logger:info_msg("registration on parking: ~p ; old parking: ~p ; Driver: ~p~n",[NewParking, State#state.parking, State#state.driver]),
			cache:do_reregister_driver(State#state.parking, NewParking, State#state.driver),
			NewState = State#state{name = main, parking=NewParking},
			{pkt:parkings_info(Head, NewState), NewState};

		{4, negative_balance} ->
			{pkt:parkings_info(Head, State), State};

		{4, S}  when S =:= main; S =:= unreg ->
			<<NewStatusI:8>> = Load,												
			NewStatus = status2a(NewStatusI),			
			%error_logger:info_msg("got new status ~p; raw status ~p from Driver#~p~n",[NewStatus, NewStatusI, (State#state.driver)#driver.id]),
			if
				State#state.status =:= NewStatus -> ok;
				true -> cache:set_driver_status((State#state.driver)#driver.id, NewStatus)
			end,
			
			case NewStatus of
				order_finished ->
					FastFinish = (cursecs() - State#state.lasttake) < finish_timeout(),
					NewSt = if FastFinish ->
									error_logger:warning_msg("attempt to finish Order: ~p too fast by Driver: ~p~n", [State#state.orderid, (State#state.driver)#driver.id]),
									State;
							   true ->		
									NState = if (State#state.driver)#driver.state =:= cant_work 
												->
													 State#state{status = NewStatus, orderid = 0, name = unreg, parking = 0};
												true ->
													 State#state{status = NewStatus, orderid = 0}
											 end,
									case cache:get_order(State#state.orderid) of
										{Pid, _} ->
											Pid ! finished,
											NState;
										no_order -> 
											error_logger:warning_msg("cant find Order: ~p just 'finished' by Driver: ~p~n", [State#state.orderid, (State#state.driver)#driver.id]),
											NState
									end			
							end,		
					{pkt:parkings_info(Head, State), NewSt};
				missing_client when State#state.orderid =/= 0, State#state.status =/= missing_client ->
					error_logger:info_msg("got new status ~p;  from Driver: ~p; sending message~n",[NewStatus, (State#state.driver)#driver.id]),
					sql ! {missing_client, {State#state.orderid, (State#state.driver)#driver.id}},
					{pkt:parkings_info(Head, State), State#state{status = NewStatus}};
				missing_client when State#state.orderid =/= 0, State#state.status =:= missing_client ->
					%error_logger:info_msg("got old status ~p;  from Driver#~p; ignoring~n",[NewStatus, (State#state.driver)#driver.id]),
					{pkt:parkings_info(Head, State), State};
				_ -> {pkt:parkings_info(Head, State), State#state{status = NewStatus}}
			 end;


		{6, S} when S =:= main; S =:= unreg ->
			<<ParkingId:8>> = Load,
			error_logger:info_msg("Driver#~p requested orders list for parking: ~p~nOrders: ~n~p~n",[(State#state.driver)#driver.id, ParkingId, cache:get_parkings_orders(ParkingId)]),
			B = case cache:get_parkings_orders(ParkingId) of
					[] -> <<>>;
					Orders -> << <<(pkt:order_offer(Head, Order, true))/binary>> ||Order <- Orders>>
				end,
			{<<B/binary,(pkt:finitor(Head))/binary>>, State};

		{7, main} when Load =:= <<0:40>> ->
			error_logger:info_msg("Driver: ~p refuses orders: ~p~n", [(State#state.driver)#driver.id, State#state.offers]),
			lists:map(fun({OrderId,Token})->
							  case cache:get_order(OrderId) of
								  {Pid, _} -> 
									  Pid ! {answer, refuse, self(), Token},
									  ok;
								  _ -> 
									  error_logger:warning_msg("cant find order to refuse from or refuse is too slow; Driver: ~p  Order: ~p~n", [(State#state.driver)#driver.id, OrderId]),
									  fail
							  end							  
					  end, State#state.offers),
			cache:do_reregister_driver(State#state.parking, State#state.parking, State#state.driver),
			{void, State#state{offers = []}};

		{7, main} when Load =/= <<0:40>>, State#state.orderid =:= 0, (State#state.driver)#driver.state=:=work ->
			<<DriverResponse:8, OrderId:32/big>> = Load,
			LegalOrder = lists:keymember(OrderId, 1, State#state.offers),
			case (cursecs() - row_timeout()) of
				Q when Q > State#state.lasttake, LegalOrder ->
					A = code2time(DriverResponse),
					{Pid, Order} = cache:get_order(OrderId),
					When = calendar:datetime_to_gregorian_seconds(erlang:universaltime())-calendar:datetime_to_gregorian_seconds({{1970,1,1},{0,0,0}}),
					{OrderId, Token} = lists:keyfind(OrderId, 1, State#state.offers),
					Pid ! {answer, A, When, (State#state.driver)#driver.id, Token},

					receive
						{acc_offer, O} when O#order.id =:= OrderId ->
							Pid ! acc_offer_ok,

							cache:do_unregister_driver(State#state.parking, State#state.driver),
							cache:set_driver_status((State#state.driver)#driver.id, working),			
							NewState = State#state{name = unreg, orderid = OrderId, status = working, parking = 0, offers= [], lasttake = cursecs()},							
							%%{void, NewState};
							%%{pkt:order_not_confirmed(Head, Order), State#state{fails = State#state.fails+1}};
							{pkt:order_confirm(Head, Order, A, When), NewState};
						{acc_offer, O} when O#order.id =/= OrderId ->
							error_logger:warning_msg("wrong order accept confirmation from order process; Driver: ~p should_Order: ~p got_Order: ~p~n",[(State#state.driver)#driver.id ,OrderId, O#order.id]),
							{pkt:order_not_confirmed(Head, Order), State#state{fails = State#state.fails+1}}
					after 10000 ->
							error_logger:warning_msg("can't wait for order accept confirmation from order process; Driver: ~p Order: ~p~n",[(State#state.driver)#driver.id ,OrderId]),
							{pkt:order_not_confirmed(Head, Order), State#state{fails = State#state.fails+1}}
					end;
					
			   _ ->
					error_logger:info_msg("attempt to take second order too quick or take illegal order~n"),
					{_Pid, Order} = cache:get_order(OrderId),
					{pkt:order_not_confirmed(Head, Order), State#state{fails = State#state.fails+1}}
					%%{void, State#state{fails = State#state.fails+1}}
			end;
		
		{8,_} ->
			{pkt:balance(Head, cache:get_new_balance((State#state.driver)#driver.id)), State};
		
		{9, _} ->			
			if is_record(State#state.lastMessage, message) ->
					error_logger:info_msg("got message confirmation, Driver: ~p Message: ~p~n",[(State#state.driver)#driver.id, (State#state.lastMessage)#message.id]),
					if is_record(State#state.lastMessage, message) ->
							cache:update_message(State#state.lastMessage);
					   true -> 
							whatever
					end;
			   true ->
					whatever
			end,																   
			{void, State#state{lastMessage = no_message}};

		Q -> 
			error_logger:warning_msg("bad opcode,state pair: ~p Driver: ~p~n",[Q, (State#state.driver)#driver.id]),
			{void, State#state{fails = State#state.fails+1}}
	end;

process_packet(Packet, State) ->
	error_logger:info_msg("failed packet from Driver: ~p~nPacket: ~p~nState: ~p~n",[(State#state.driver)#driver.id, Packet, State]),
	{void, State#state{fails = State#state.fails+1}}.

