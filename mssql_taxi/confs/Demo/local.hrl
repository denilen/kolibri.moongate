connect_string() -> "DSN=demo;UID=demo;PWD=demo".
sort_zones_by_id() -> true.	    %сортировать ли стоянки  по айдишникам; по алфавиту в противном случае.
show_all_drivers() -> false.	%показывать клиенту всех водителей в распределении или только своих.
econom_logic() -> true.		    %отрабатывать ли специальную логику для эконома. true|false
predvar_time() -> 30.		    %minutes - за какое время до выполнения активировать предварительный заказ
port() -> 40000.

% Client notification
client_preliminary_notification_method() -> sms_phone.  %sms_phone|sms_no_phone|call|off  Метод предварительного (при взятии заказа) оповещения.
client_missing_notification_method() -> sms_phone.      %sms_phone|sms_no_phone|call|off  Метод приглашения клиента.

% Лицензирование
key() -> "demo".    %имя запрашиваемого ключа лицензии
