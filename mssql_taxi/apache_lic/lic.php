<?php 
function pkcs1_pad($d,$blocksize){
		 $l=$blocksize - strlen($d)-3;
		 if($l < 8) return '';
		 $res =chr(0).chr(2);
		 for($i=0; $i<$l ; $i++){
		 		   $res .= chr(rand(1,255));
		 }
		 return $res.chr(0).$d;		 
}

header('Content-Type:application/binary');

$licname= $_REQUEST['key'].'.lic';
$data=file_get_contents($licname);
if ($data == FALSE) exit();
$pdata = pkcs1_pad($data,4096/8);

$tfn = tempnam('./temp','tmp');
//echo $tfn;
$tf = fopen($tfn,'w');
fwrite($tf, $pdata);
fflush($tf);
fclose($tf);

chmod($tfn, 0644);

echo shell_exec('openssl rsautl -decrypt -raw -inkey rsa_privatekey.pem -in '.$tfn.'');
unlink($tfn);
?>

